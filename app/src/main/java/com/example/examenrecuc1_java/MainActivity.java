package com.example.examenrecuc1_java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

private EditText txtNombreCliente;
private Button btnCotización;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnCotización.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });
    }

    private void iniciarComponentes(){
        txtNombreCliente = (EditText) findViewById(R.id.txtNombreCliente);
        btnCotización = (Button)  findViewById(R.id.btnCotización);
    }

    private  void entrar(){
        String nombre = txtNombreCliente.getText().toString().trim();

        if (nombre.isEmpty()) {
            Toast.makeText(this.getApplicationContext(), "Ingrese el nombre del cliente", Toast.LENGTH_SHORT).show();
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNombreCliente.getText().toString());


            Intent intent = new Intent(MainActivity.this, cotizacionActivity.class);
            intent.putExtras(bundle);

            startActivity(intent);
        }
    }
}
