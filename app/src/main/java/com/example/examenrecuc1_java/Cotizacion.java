package com.example.examenrecuc1_java;

public class Cotizacion {
    // Declaración de variables de clase
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    // Métodos públicos
    public Cotizacion(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    // Set & Get
    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getNumRecibo() {
        return this.numRecibo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabNormal() {
        return this.horasTrabNormal;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public float getHorasTrabExtras() {
        return this.horasTrabExtras;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getPuesto() {
        return this.puesto;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public float getImpuestoPorc() {
        return this.impuestoPorc;
    }

    // Subtotal
    public float calcularMensualidad(int puesto, float horasTrabNormal,float horasTrabExtras) {
        float pagoBase = 200;
        float total = 0;
        float enganche = (float)((float)((horasTrabNormal)*(horasTrabExtras/100)));
        float totalSinEnganche = horasTrabNormal - enganche;
        // Calculando el pago base según el puesto
        if (puesto == 1) {
            pagoBase = (float) (totalSinEnganche/12);
        } else if (puesto == 2) {
            pagoBase = (float) (totalSinEnganche/18);
        } else if (puesto == 3) {
            pagoBase = (float) (totalSinEnganche/24);
        } else if (puesto == 4) {
            pagoBase = (float) (totalSinEnganche/36);
        } else {
            System.out.println("No se puede calcular");
        }

        return pagoBase;

    }

    public float calcularEnganche( float horasTrabNormal,float horasTrabExtras) {
        float impuesto = (float)((float)((horasTrabNormal)*(horasTrabExtras/100)));;
        return impuesto;

    }



    public float calcularTotal(float horasTrabNormal,float horasTrabExtras) {
        float enganche = (float)((float)((horasTrabNormal)*(horasTrabExtras/100)));
        float totalPagar = horasTrabNormal - enganche;;
        return totalPagar;
    }
}
